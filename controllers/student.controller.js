const fs = require("fs");
const path = require("path");
const Student = require("../models/Student");
const { transformPayload } = require("../helpers/");

const xmlPath = path.join(__dirname, "..", "data", "data.xml");
const xmlData = fs.readFileSync(xmlPath, "utf8");

/**
 *  Add new student using xml data
 * @param {*} req
 * @param {*} res
 */
exports.register = (req, res) => {
  const transformedData = transformPayload(xmlData);

  const { first_name, last_name, email, address } = transformedData.root;

  req.body.email = email._text;
  req.body.first_name = first_name._text;
  req.body.last_name = last_name._text;
  req.body.address = {
    city: address.city._text,
    street: address.street._text,
  };

  this.addToDb(req, res);
};

exports.addToDb = (req, res) => {
  Student.findOne({ email: req.body.email }).exec((_err, student) => {
    // if email is already taken
    if (student) {
      return res.status(400).json({ error: "Email is already registered." });
    }

    let { first_name, last_name, email, address } = req.body;

    let newStudent = new Student({ first_name, last_name, email, address });

    newStudent.save((err, _) => {
      if (err) {
        return res.status(400).json({ error: err });
      }

      return res.json(newStudent);
    });
  });
};

/**
 * Read student data from the database
 * @param {*} req
 * @param {*} res
 */
exports.read = (req, res) => {
  const { _id } = req.params;
  Student.findOne({ _id }).exec((err, student) => {
    if (err) {
      return res.json({
        error: "Error fetching student data: " + err,
      });
    }

    // If no matching record is found
    if (!student) {
      return res.status(404).json({
        error: "Student not found.",
      });
    }
    return res.json(student);
  });
};

/**
 * Get all students from the database
 *
 * @param {*} _req
 * @param {*} res
 */
exports.list = (_req, res) => {
  Student.find({}).exec((err, data) => {
    if (err) {
      return res.json({
        error: "Error fetching students.",
      });
    }
    res.json(data);
  });
};

/**
 * Update student record
 *
 * @param {*} req
 * @param {*} res
 */
exports.updatePartially = (req, res) => {
  const { _id } = req.params;
  Student.findOne({ _id }).exec((err, oldStudent) => {
    if (err) {
      return res.status(400).json({
        error: "Error retrieving student data: " + err,
      });
    }

    // If no matching record is found
    if (!oldStudent) {
      return res.status(404).json({
        error: "Student not found.",
      });
    }

    const { first_name, last_name, address } = req.body;

    if (first_name) {
      oldStudent.first_name = first_name;
    }

    if (last_name) {
      oldStudent.last_name = last_name;
    }

    if (address) {
      oldStudent.address = address;
    }

    oldStudent.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: "Error updating student.",
        });
      }
      res.json(result);
    });
  });
};

/**
 * Update student record partially
 * @param {*} req
 * @param {*} res
 */
exports.update = (req, res) => {
  // Check if student exists
  const { _id } = req.params;
  Student.findOne({ _id }).exec((err, oldStudent) => {
    if (err) {
      return res.status(400).json({
        error: "Error retrieving student data: " + err,
      });
    }

    // If no matching record is found
    if (!oldStudent) {
      return res.status(404).json({
        error: "Student not found.",
      });
    }

    const { first_name, last_name, email, address } = req.body;

    oldStudent.first_name = first_name;
    oldStudent.last_name = last_name;
    oldStudent.email = email;
    oldStudent.address = address;

    oldStudent.save((err, _) => {
      if (err) {
        return res
          .status(400)
          .json({ error: "Error updating student record: " + err });
      }
      return res.json(oldStudent);
    });
  });
};
