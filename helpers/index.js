const convert = require("xml-js");

exports.transformPayload = (xml) => {
  const result = convert.xml2js(xml, { compact: true, spaces: 4 });
  return result;
};
