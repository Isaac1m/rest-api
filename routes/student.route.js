const express = require("express");
const router = express.Router();

const {
  register,
  update,
  list,
  read,
  updatePartially,
} = require("../controllers/student.controller");

router.post("/student", register);
router.put("/student/:_id", update);
router.patch("/student/:_id", updatePartially);
router.get("/students", list);
router.get("/student/:_id", read);

module.exports = router;
