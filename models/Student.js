const mongoose = require("mongoose");

const addressSchema = mongoose.Schema({
  city: String,
  street: String,
});

const studentSchema = new mongoose.Schema(
  {
    first_name: {
      type: String,
      required: true,
      trim: true,
    },
    last_name: {
      type: String,
      required: true,
      trim: true,
    },
    address: {
      type: addressSchema,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Student", studentSchema);
