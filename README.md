
## Set up instructions

1. Clone the repository onto your machine.
2. cd into the root folder (rest-api)
3. Run ```npm install``` to install project dependencies
4. If you want to use a different MongoDB instance, change the DB property in the .env file. 
5. Run ```npm start``` to start the server. 






## Config

The .env file contains the following configurations.

1. `CLIENT_URL` client app's url to avoid it being blocked by cors.


2.  `DB` url of the MongoDB instance the server connects to.


3. `PORT`  ` port on which the server will be listening.

