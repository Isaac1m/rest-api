  -- Question 1

  SELECT c.course_id, c.course_name FROM course_main c 
  JOIN course_users cu ON c.pk = cu.course_main_pk
  JOIN users u ON cu.users_pk=u.pk
  WHERE u.user_id='jmclemon6' ORDER BY c.course_id ASC

  -- Question 2

SELECT u.user_id, u.first_name,u.last_name
FROM course_users cu
JOIN users u 
 ON(cu.users_pk = u.pk)
JOIN course_main cm ON cu.course_main_pk = cm.pk
WHERE cm.course_id IN('HCS2022','RUS128')
GROUP BY u.user_id, u.first_name,u.last_name
HAVING COUNT(distinct cu.course_main_pk) = 2

-- Question 3


SELECT u.user_id, u.first_name,u.last_name, u.email, u.pk
FROM course_users cu
JOIN users u 
 ON(cu.users_pk = u.pk)
JOIN course_main cm ON cu.course_main_pk = cm.pk
WHERE cm.course_id IN('BIE103','CHM1011')
GROUP BY u.user_id, u.first_name,u.last_name, u.email, u.pk
HAVING COUNT(distinct cu.course_main_pk) = 1

-- Question 4

SELECT u.user_id, u.first_name,u.last_name, u.email, u.pk
FROM course_users cu
JOIN users u 
 ON(cu.users_pk = u.pk)
JOIN course_main cm ON cu.course_main_pk = cm.pk
GROUP BY u.user_id, u.first_name,u.last_name, u.email, u.pk
HAVING COUNT(distinct cu.course_main_pk) >=1

-- Question 5

SELECT u.user_id, c.course_id FROM course_main c 
  JOIN course_users cu ON c.pk = cu.course_main_pk
  JOIN users u ON cu.users_pk=u.pk
  WHERE c.term LIKE 'SPRING%' 


-- Question 6
SELECT u.user_id,u.first_name, u.last_name, c.course_name FROM course_main c 
  JOIN course_users cu ON c.pk = cu.course_main_pk
  JOIN users u ON cu.users_pk=u.pk
  ORDER BY u.user_id ASC
