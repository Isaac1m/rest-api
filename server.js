const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
require("dotenv").config();

const studentRoutes = require("./routes/student.route");

const app = express();

// To parse the incoming requests with JSON payloads
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// connect to db

mongoose
  .connect(process.env.DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log(`Connected to db.`);
  })
  .catch((err) => {
    console.log(`Error connecting to db: ${err}`);
  });

app.use(cors({ origin: `${process.env.CLIENT_URL}` }));

// Add routes
app.use("/api", studentRoutes);

const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log(`Server started on ${port}`);
});
